package unused;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;





public class FileUpload {

	
	public static void fileUpload(String localFile, String remoteFilename){
		WebContentHandler handler = new WebContentHandler();
		
		String url = "http://highscores.ch/tippspiel/upload.php";
		String hash = "FI!DOiRJfM6yGYayLg4grEV735f3pqUUK1xN4qPI0j0!pjcBYJur6e4bcS7";
//		
		String html = handler.fetchPageGet(url, 0);
		
		System.out.println(html);
		
//		
//	     // Compose the request header
//        StringBuffer buf = new StringBuffer();
//        buf.append("POST ");
//        buf.append(uploader.getUploadAction());
//        buf.append(" HTTP/1.1\r\n");
//        buf.append("Content-Type: multipart/form-data; boundary=");
//        buf.append(boundary);
//        buf.append("\r\n");
//        buf.append("Host: ");
//        buf.append(uploader.getUploadHost());
//        buf.append(':');
//        buf.append(uploader.getUploadPort());
//        buf.append("\r\n");
//        buf.append("Connection: close\r\n");
//        buf.append("Cache-Control: no-cache\r\n");
//
//        // Add cookies
//        List cookies = uploader.getCookies();
//        if (!cookies.isEmpty())
//            {
//                buf.append("Cookie: ");
//                for (Iterator iterator = cookies.iterator(); iterator.hasNext(); )
//                    {
//                        Parameter parameter = (Parameter)iterator.next();
//
//                        buf.append(parameter.getName());
//                        buf.append('=');
//                        buf.append(parameter.getValue());
//
//                        if (iterator.hasNext())
//                            buf.append("; ");
//                    }
//
//                buf.append("\r\n");
//            }
//
//        buf.append("Content-Length: ");
//
//        // Request body
//        StringBuffer body = new StringBuffer();
//        List fields = uploader.getFields();
//        for (Iterator iterator = fields.iterator(); iterator.hasNext();)
//            {
//
//                Parameter parameter = (Parameter) iterator.next();
//
//                body.append("--");
//                body.append(boundary);
//                body.append("\r\n");
//                body.append("Content-Disposition: form-data; name=\"");
//                body.append(parameter.getName());
//                body.append("\"\r\n\r\n");
//                body.append(parameter.getValue());
//                body.append("\r\n");
//            }
//
//        body.append("--");
//        body.append(boundary);
//        body.append("\r\n");
//        body.append("Content-Disposition: form-data; name=\"");
//        body.append(uploader.getImageFieldName());
//        body.append("\"; filename=\"");
//        body.append(file.getName());
//        body.append("\"\r\n");
//        body.append("Content-Type: image/pjpeg\r\n\r\n");
//
//        String boundary = "WHATEVERYOURDEARHEARTDESIRES";
//        String lastBoundary = "\r\n--" + boundary + "--\r\n";
//        long length = file.length() + (long) lastBoundary.length() + (long) body.length();
//        long total = buf.length() + body.length();
//
//        buf.append(length);
//        buf.append("\r\n\r\n");
//
//         Upload here
//        InetAddress address = InetAddress.getByName(uploader.getUploadHost());
//        Socket socket = new Socket(address, uploader.getUploadPort());
//        try
//            {
//                socket.setSoTimeout(60 * 1000);
//                uploadStarted(length);
//
//                PrintStream out = new PrintStream(new BufferedOutputStream(socket.getOutputStream()));
//                out.print(buf);
//                out.print(body);
//
//                // Send the file
//                byte[] bytes = new byte[1024 * 65];
//                int size;
//                InputStream in = new BufferedInputStream(new FileInputStream(file));
//                try
//                    {
//                        while ((size = in.read(bytes)) > 0)
//                            {
//                                total += size;
//                                out.write(bytes, 0, size);
//                                transferred(total);
//                            }
//                    }
//                finally
//                    {
//                        in.close();
//                    }
//
//                out.print(lastBoundary);
//                out.flush();
//
//                // Read the response
//                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//                while (reader.readLine() != null);
//            }
//        finally
//            {
//                socket.close();
//            }

		
//		String url = "https://chopin.metanet.ch:8443/sitepreview/http/highscores.ch/tippspiel/upload.php";
//		String hash = "FI!DOiRJfM6yGYayLg4grEV735f3pqUUK1xN4qPI0j0!pjcBYJur6e4bcS7";
//		
//		
//		HttpClient httpclient = new DefaultHttpClient();
//	    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//
//	    HttpPost httppost = new HttpPost(url + "?hash="+hash+"&filename="+remoteFilename);
//	    File file = new File(localFile);
//
//	    FileEntity reqEntity = new FileEntity(file, "binary/octet-stream");
//
//	    httppost.setEntity(reqEntity);
//	    reqEntity.setContentType("binary/octet-stream");
//	    System.out.println("executing request " + httppost.getRequestLine());
//	    HttpResponse response = httpclient.execute(httppost);
//	    HttpEntity resEntity = response.getEntity();
//
//	    System.out.println(response.getStatusLine());
//	    if (resEntity != null) {
//	      System.out.println(EntityUtils.toString(resEntity));
//	    }
//	    if (resEntity != null) {
//	      resEntity.consumeContent();
//	    }
//
//	    httpclient.getConnectionManager().shutdown();
	  }
	
	public static void upload(String localFile, String remoteFilename) throws InterruptedException, IOException {
		String url = "https://chopin.metanet.ch:8443/sitepreview/http/highscores.ch/tippspiel/upload.php";
		String hash = "FI!DOiRJfM6yGYayLg4grEV735f3pqUUK1xN4qPI0j0!pjcBYJur6e4bcS7";
		
		HttpURLConnection httpUrlConnection = (HttpURLConnection)new URL(url + "?hash="+hash+"&filename="+remoteFilename).openConnection();
        httpUrlConnection.setDoOutput(true);
        httpUrlConnection.setRequestMethod("POST");
        OutputStream os = httpUrlConnection.getOutputStream();
        Thread.sleep(1000);
        BufferedInputStream fis = new BufferedInputStream(new FileInputStream(localFile));
        DataInputStream dis = new DataInputStream(fis);

        
        while (dis.available() != 0) {
            os.write(dis.read());
        }

        os.close();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                httpUrlConnection.getInputStream()));

        String s = null;
        while ((s = in.readLine()) != null) {
            System.out.println(s);
        }
        in.close();
        fis.close();
	}
}
