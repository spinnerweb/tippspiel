package unused;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;

public class FTP {

	public static void uploadData(String localFilename, String remoteFilename) {
		
		FTPClient client = new FTPClient();
        FileInputStream fis = null;
        
        
        try {
        	 client.connect("chopin.metanet.ch");
            ;
            
            if (client.login("tippspiel", "diomio2012"))
            {
            	client.enterLocalPassiveMode();
            	// Create an InputStream of the file to be uploaded
                fis = new FileInputStream(localFilename);
     
                // Store file to server
                if (client.storeFile(remoteFilename, fis))
                    System.out.println("File "+remoteFilename+" successfully stored");
                else
                    System.err.println("File "+remoteFilename+" could not be stored: "+client.getReplyString());
                	
                
            } else {
            	System.err.println("Login failed");
            }
 
            client.logout();
            
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
	
}
