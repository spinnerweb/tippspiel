package unused;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class WebContentHandler {
	private final static String END_OF_INPUT = "\\Z";
	
	//private String cookies;
	private static boolean debugMode = false;
	
	// TODO check if really needed
	public static final int PROXY_MODE_DISABLED  = 0;
	public static final int PROXY_MODE_RECORDING = 1;
	public static final int PROXY_MODE_REPLAY 	 = 2;
	
	private int proxyMode = PROXY_MODE_DISABLED;
	
	private static final int CONNECT_TIMEOUT = 10000;
	private static final int READ_TIMEOUT    = 45000;
	
	
	private ArrayList<String> userAgents;
	private ArrayList<Hashtable<String, String>> cookies;
	private String lastUrl;
	private String lastUrlNoFollow;
	
	public static final int PROXY_LOCAL 	= 0;		// just local connection
	
	private static boolean hasInitAll = false;
	
//	private static long maxTimeSpent = 0;
	
	
	// List of possible user-agents.
	// Source: http://www.useragentstring.com/
	private static final String[] availableUserAgents = {
		"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.6) Gecko/20100625 YFF35 Firefox/3.6.6"
	   ,"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.19) Gecko/20081202 Firefox (Debian-2.0.0.19-0etch1)"
	   ,"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.16) Gecko/20110323 Ubuntu/10.10 (maverick) Firefox/3.6.16"
	   ,"Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322)"
	   ,"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us) AppleWebKit/534.1+ (KHTML, like Gecko) Version/5.0 Safari/533.16"
	   ,"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.4; en; rv:1.9.0.19) Gecko/2010051911 Camino/2.0.3 (like Firefox/3.0.19)"
	};
	
	/**
	 * constructor
	 * @param proxyCallback add SinglotenServer in here.
	 */
	public WebContentHandler() {
		super();
		
		cookies = new ArrayList<Hashtable<String, String>>();
		userAgents = new ArrayList<String>();
	}
	
	private String getUserAgent(int proxyId) {
		if (userAgents.size() <= proxyId) {
			for (int i=userAgents.size(); i<=proxyId; i++)
				userAgents.add(getRandomUserAgent());
		
		}
		return userAgents.get(proxyId);
	}
	
	public void setUserAgent(int proxyId, String userAgent) {
		if (userAgents.size() <= proxyId) {
			for (int i=userAgents.size(); i<=proxyId; i++) {
				userAgents.add(getRandomUserAgent());
			}
		}
		
		userAgents.set(proxyId, userAgent);
	}
	
	private Hashtable<String, String> getCookies(int proxyId) {
		if (cookies.size() <= proxyId) {
			for (int i=cookies.size(); i<=proxyId; i++)
				cookies.add(new Hashtable<String, String>());
		
		}
		return cookies.get(proxyId);
	}
	
	private void setCookie(String cookie, int proxyId) {
		String[] parts = cookie.split("=", 2);
		if (parts.length == 2) {
			setCookie(parts[0].trim(), parts[1].trim(), proxyId);
		}
	}
	
	public String getCookie(String name, int proxyId) {
		return getCookies(proxyId).get(name);
	}
	
	public void setCookie(String name, String value, int proxyId) {
		// set the cookie now
		getCookies(proxyId).put(name, value);
	}
	
	public String getCookiesAsString(int proxyId) {
		String cookieString = "";
		
		Set<Entry<String, String>> set = getCookies(proxyId).entrySet();
		Iterator<Entry<String, String>> it = set.iterator();
		
		boolean first = true;
		
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			if (first)
				first = false;
			else
				cookieString = cookieString + "; ";
			
			// append cookie key/value pair, if value is not empty
			if (!entry.getValue().isEmpty())
				cookieString = cookieString + entry.getKey() + "=" + entry.getValue();
		}
		
		
		return cookieString;
	}
	
	private String getRandomUserAgent() {
		int index = new Double(Math.floor(Math.random() * availableUserAgents.length)).intValue();
		return availableUserAgents[index];
	}

	
//	private String getCookie(String name, int proxyId) {
//		Set<Entry<String, String>> set = getCookies(proxyId).entrySet();
//		Iterator<Entry<String, String>> it = set.iterator();
//		
//		while (it.hasNext()) {
//			Entry<String, String> entry = it.next();
//			
//			if (entry.getKey().toLowerCase().equals(name.toLowerCase())) {
//				return entry.getValue();
//			}
//		}
//		return null;	
//	}
	
	public void clearCookies() {
		cookies.clear();
	}
	
	public String fetchPageGet(String url, int bookie) {
		return fetchPage(url, false, null, false, null, PROXY_LOCAL, null, bookie, null, true, false, false, 0);
	}
	
	public String fetchPageGet(String url, String referrer, int bookie) {
		return fetchPage(url, false, null, false, null, PROXY_LOCAL, referrer, bookie, null, true, false, false, 0);
	}
	
	public String fetchPageGetWithProxy(String url, int bookie, int proxy) {
		return fetchPage(url, false, null, false, null, proxy, null, bookie, null, true, false, false, 0);
	}

	public String fetchPageWithCookies(String url, String additionalCookies, boolean doKeepOtherCookies, boolean doPost, String postParameters, int bookie) {
		if (!doKeepOtherCookies) {
			clearCookies();
		}
		
		if (additionalCookies != null && !additionalCookies.isEmpty()) {
			// Split cookies and add them
			String[] parts = additionalCookies.split(";");
			for (int i=0; i<parts.length; i++) {
				setCookie(parts[i].trim(), PROXY_LOCAL);
			}
		}
		return fetchPage(url, doPost, postParameters, false, null, PROXY_LOCAL, null, bookie, null, true, false, false, 0);
	}
	
	public String fetchPagePost(String url, String postParameters, int bookie) {
		return fetchPage(url, true, postParameters, false, null, PROXY_LOCAL, null, bookie, null, true, false, false, 0);
	}
	
	public String fetchPagePostWithReferer(String url, String postParameters, int bookie, String referer) {
		return fetchPage(url, true, postParameters, false, null, PROXY_LOCAL, referer, bookie, null, true, false, false, 0);
	}
	
	/**
	 * Fetch a page according to the given parametrs
	 * @param url					url to fetch
	 * @param doPost				POST (true) or GET (false)
	 * @param postParameters		parameters to post, be sure to call URLEncoder.encode(x, "UTF-8") on values. 
	 * @param isXMLHttpRequest		adds property XMLHttpRequest
	 * @param content type			override default content type if not null
	 * @param proxy					proxy id to use, -1 if no proxy is to be used
	 * @param referer				url of the referer, or null if no referer should be used
	 * @param bookie				Bookie ID
	 * @param cookieOverride		Override complete cookie string with this parameter. if regular cookie required, use null.
	 * @param doFollow				Follow request to forward address with same parameters. 
	 * 								If false, will not follow, but still set lastUrl
	 * @param isMobileUserAgent		overrides the normal user agent with a mobile one
	 * @return Content of page
	 */
	public String fetchPage(String url, boolean doPost, String postParameters, boolean isXMLHttpRequest, String contentType, int proxy, String referer, int bookie, String cookieOverride, boolean doFollow, boolean ignoreEOFExceptions, boolean isMobileUserAgent) {
		return fetchPage(url, doPost, postParameters, isXMLHttpRequest, contentType, proxy, referer, bookie, cookieOverride, doFollow, ignoreEOFExceptions, isMobileUserAgent, 0);
	}
	
	private String fetchPage(String url, boolean doPost, String postParameters, boolean isXMLHttpRequest, String contentType, int proxy, String referer, int bookie, String cookieOverride, boolean doFollow, boolean ignoreEOFExceptions, boolean isMobileUserAgent, int doFollowDepth) {
		lastUrl = url;
		if (doPost && postParameters!=null && postParameters.length()>0) {
		}
		URL urlObj;
		String html = new String();
		
		try {
			if (proxy == PROXY_LOCAL) {
				urlObj = new URL(url);
			}
			else {
				try {
					urlObj = new URL(URLEncoder.encode(url, "UTF-8"));
				}
				catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					urlObj = new URL(url);
				}
			}
			
			// Init settings
			initAll();

			HttpURLConnection connection = null;
			
			try {
				//if (bookie == Bookie.BOOKIE_PINNACLE && urlObj.getProtocol().toLowerCase().equals("https")) {
				//	// HACK: Trust any certificate
				//	// TODO remove
				//	System.out.println("pinnacle special case");
				//	connection = (HttpsURLConnection)urlObj.openConnection();
				//	((HttpsURLConnection)connection).setHostnameVerifier(new VerifyEverythingHostnameVerifier());
				//} else { 
				connection = (HttpURLConnection)urlObj.openConnection();
				//}
				connection.setInstanceFollowRedirects(false);
				
				// Set the properties
				connection.setRequestProperty("Cookie", (cookieOverride!=null)?cookieOverride:getCookiesAsString(proxy));
				connection.setRequestProperty("User-Agent", getUserAgent(proxy));
				connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				//connection.setRequestProperty("Accept-Encoding", "gzip,deflate");
				connection.setRequestProperty("Accept-Language", "en"); 
				connection.setRequestProperty("Pragma", "no-cache");
				
				if (isXMLHttpRequest){
					connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

					if (contentType == null)
						contentType = "application/json; charset=utf-8";
							
					// application/x-www-form-urlencoded
					connection.setRequestProperty("Content-Type", contentType);
					
				// for gamebookers:	connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded; charset=UTF-8"); //"application/json; charset=utf-8");
				} else if (doPost) {
					if (contentType == null)
						contentType = "application/x-www-form-urlencoded";
					connection.setRequestProperty("Content-Type",contentType);
				}
				
				
				connection.setUseCaches(false);
				//  connection.setRequestProperty("Host", "www.something_random.com");
				//connection.setRequestProperty("referer", url);
				
				if (referer != null) {
					connection.setRequestProperty("Referer", referer);
				}
				
				if (isDebugMode()){
					Object[] keys = connection.getRequestProperties().keySet().toArray();
					
					for(int i=0; i<keys.length; i++) {
						String valueString = "";
						Object[] values = connection.getRequestProperties().get(keys[i]).toArray();
						
						for (int j=0; j<values.length; j++) {
							if (j>0)
								valueString +="; ";
							valueString += values[j];
						}
					}
				}
				
//				long timeStart = new Date().getTime();
				
				Map<String, List<String>> headers = connection.getRequestProperties();
				  
				  
				if (!doPost) {
					// Read content with GET request method
					boolean useScanner = true;
					if (!useScanner) {
						html = getResult(connection, bookie, ignoreEOFExceptions);
						//html = getResultBytes(connection);
					}
					else {
						String ctype = connection.getContentType();
						int csi = 0;
						if (ctype != null) {
							csi = ctype.indexOf("charset=");
						}
						Scanner scanner = null;
						try {
							String encoding=connection.getHeaderField("Content-Encoding");
							if (encoding!=null && encoding.equals("gzip")) {
								scanner = new Scanner(new InputStreamReader(new java.util.zip.GZIPInputStream(connection.getInputStream())));
							}
							else if (csi > 0) {
								scanner = new Scanner(new InputStreamReader(connection.getInputStream(), ctype.substring(csi + 8)));
							}
							else {
								scanner = new Scanner(new InputStreamReader(connection.getInputStream()));
							}
							scanner.useDelimiter(END_OF_INPUT);
							try {
								html = scanner.next();
							} catch (Exception e){
								// ignore error
							}
						}
						finally {
							if (scanner != null)
								scanner.close();
						}
						
						
					}
				}
				else {
					OutputStreamWriter wr = null;
					try { 
						// Send data 
						connection.setDoOutput(true); 
						wr = new OutputStreamWriter(connection.getOutputStream()); 
						wr.write(postParameters); 
						wr.flush(); 
						
						// Get the response (deflate gzip if necessary)
						html = getResult(connection, bookie, ignoreEOFExceptions);
						
					} catch (EOFException e) {
						if (!ignoreEOFExceptions) { 
							System.err.println("Error within WebContentHandler, streamwriter: ");
							e.printStackTrace();
						}
					} catch (Exception e) {
						System.err.println("Error within WebContentHandler, streamwriter: ");
						e.printStackTrace();
						
					} finally {
						if (wr != null) {
							wr.close(); 
						}
					}
				}
				
//				long diff = new Date().getTime() - timeStart;
//				
//				if (diff > maxTimeSpent) {
//					System.err.println("New max time spent: "+diff+" ms, "+Bookie.bookieString(bookie)+", url: "+url);
//					maxTimeSpent = diff;
//				}
		    	
			  

				
				// Parse cookies & location
				String headerName=null;
				String location = null;
				for (int i=1; (headerName = connection.getHeaderFieldKey(i))!=null; i++) {
				 	if (headerName.equals("Set-Cookie")) {                  
				 		String cookie = connection.getHeaderField(i);
				 		cookie = cookie.substring(0, cookie.indexOf(";"));
				 		setCookie(cookie, proxy);
				 	} else if (headerName.equals("Location")) {
				 		location = connection.getHeaderField(i);
				 	}
				}
				
				// Check if this page is a redirect and if so, follow it
				if (location != null){
					if (!location.substring(0,4).toLowerCase().equals("http")) {
						if (location.charAt(0) != '/') {
							// This is a relative url, so add the path
							String parts[] = url.split("\\?");
							int lastSlash = parts[0].lastIndexOf('/');
							
							location = parts[0].substring(0,lastSlash+1) + location;
							
						} else {
							// Relative url given, so add domain as a prefix
							location = urlObj.getProtocol() + "://" + urlObj.getHost() + location;
						}
						
					}
					if (doFollow && doFollowDepth < 5)
						return fetchPage(location, false, null, false, contentType, proxy, null, bookie, cookieOverride, doFollow, ignoreEOFExceptions, isMobileUserAgent, doFollowDepth + 1);
					else {
						lastUrl = location;
						lastUrlNoFollow = location;
					}
					
				} else {
					return html;
				}

			} catch (IOException e) {
				System.err.println("Error reading " + urlObj);
				e.printStackTrace();
				
			} 

		} catch (MalformedURLException e1) {
		} // would have to handle MalformedURLException
		return null;
	}
	
	private String getResult(HttpURLConnection connection, int bookie, boolean ignoreEOFExceptions) throws IOException {
		String encoding=connection.getHeaderField("Content-Encoding");
		String html = "";
		try {
			if (encoding!=null && encoding.equals("gzip")) {
				GZIPInputStream gz = new java.util.zip.GZIPInputStream(connection.getInputStream());
				byte[] contents = new byte[4096];
				int bytesRead=0;
				while((bytesRead = gz.read(contents)) != -1){
					html += new String(contents, 0, bytesRead);
				}
				gz.close();
			}
			else {
				InputStreamReader is = new InputStreamReader(connection.getInputStream());
				char[] contents = new char[4096];
				int bytesRead = is.read(contents);
				while(bytesRead != -1){
					html += new String(contents, 0, bytesRead);
					bytesRead = is.read(contents);
				}
				is.close(); 
			}
		} catch (IOException e) {
			if (!ignoreEOFExceptions) {
				if (connection.getErrorStream() != null) {
					// Let's try to get the ErrorStream instead and log it
					if (encoding!=null && encoding.equals("gzip")) {
						GZIPInputStream gz = new java.util.zip.GZIPInputStream(connection.getErrorStream());
						byte[] contents = new byte[4096];
						int bytesRead=0;
						while((bytesRead = gz.read(contents)) != -1){
							html += new String(contents, 0, bytesRead);
						}
						gz.close();
					}
					else {
						
							InputStreamReader is = new InputStreamReader(connection.getErrorStream());
							char[] contents = new char[4096];
							int bytesRead = is.read(contents);
							while(bytesRead != -1){
								html += new String(contents, 0, bytesRead);
								bytesRead = is.read(contents);
							}
							is.close(); 
					}
				}
				System.err.println("WEBCONTENTHANDLER: ERROR-PAGE: \n"+html);
				throw e;
			}
		}
		return html;
	}
	
	
	
//	private String getResultBytes(URLConnection connection) throws IOException {
//		String encoding=connection.getHeaderField("Content-Encoding");
//		String html = "";
//		int size = 50000;
//		int length = 0;
//		if (encoding!=null && encoding.equals("gzip")) {
//			byte[] res = new byte[size];
//			GZIPInputStream gz = new java.util.zip.GZIPInputStream(connection.getInputStream());
//			byte[] contents = new byte[4096];
//			int bytesRead=0;
//			while((bytesRead = gz.read(contents)) != -1){
//				//res[bytesRead] = contents.clone();
//				length += bytesRead;
//				if (length >= size) {
//					// duplicate res
//					html += new String(res, 0, length-bytesRead);
//					res = new byte[size];
//				}
//				System.arraycopy(contents, 0, res, 0, bytesRead);
//			}
//			gz.close();
//			return html;
//		}
//		else {
//			InputStreamReader is = new InputStreamReader(connection.getInputStream());
//			char[] contents = new char[4096];
//			int bytesRead=0;
//			char[] res = new char[size];
//			while((bytesRead = is.read(contents)) != -1){
//				if ((length+bytesRead) >= size) {
//					// duplicate res
//					html += new String(res, 0, length);
//					length = 0;
//					res = new char[size];
//				}
//				length += bytesRead;
//				System.arraycopy(contents, 0, res, 0, bytesRead);
//			}
//			is.close();
//			return html;
//		}
//	}
	
	public static boolean isDebugMode() {
		return debugMode;
	}

	public static void setDebugMode(boolean newDebugMode) {
		debugMode = newDebugMode;
	}

	public String getLastUrl() {
		return lastUrl;
	}
	
	public String getLastUrlNoFollow() {
		return lastUrlNoFollow;
	}

	public synchronized void setLastUrl(String lastUrl) {
		this.lastUrl = lastUrl;
	}
	
	private static void initAll() {
		if (!hasInitAll) {
			Properties systemProperties = System.getProperties();
			systemProperties.setProperty("sun.net.client.defaultConnectTimeout",Integer.toString(CONNECT_TIMEOUT));
			systemProperties.setProperty("sun.net.client.defaultReadTimeout",Integer.toString(READ_TIMEOUT));
			
			
			// HACK: Trust all certificates
			TrustManager[] trustManager = new TrustManager[] {new TrustEverythingTrustManager()};

		    // Let us create the factory where we can set some parameters for the connection
		    SSLContext sslContext = null;
		    try {
		        sslContext = SSLContext.getInstance("SSL");
		        sslContext.init(null, trustManager, new java.security.SecureRandom());
		    } catch (NoSuchAlgorithmException e) {
		        // do nothing
		    }catch (KeyManagementException e) {
		        // do nothing
		    }

		    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			
			
			hasInitAll = true;
		}
	}
	
	// TODO remove
	// HACK: Trust all certificates
	public static class TrustEverythingTrustManager implements X509TrustManager {
	    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	        return null;
	    }

	    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {   }

	    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {   }
	}
	

    public static class VerifyEverythingHostnameVerifier implements HostnameVerifier {

        public boolean verify(String string, SSLSession sslSession) {
            return true;
        }
    }
}