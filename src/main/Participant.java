package main;

public class Participant implements Comparable<Participant>{

	public enum CompareMode {
		SCORES, NAMES
	};
	
	private static CompareMode compareMode = CompareMode.NAMES;
	
	private final String name;
	private final int work_nr;
	private final boolean paid;
	private final Team[] teams;
	
	private double points = 0.0;
	private double winnings = 0.0;
	
	
	// MK ICE CREAM
	private boolean hasMKIceCream = false;
	
	public Participant(String name, int work_nr, boolean paid, Team team1, Team team2, Team team3, Team team4, Team team5) {
		this.name = name;
		this.work_nr = work_nr;
		this.paid = paid;
		
		this.teams = new Team[]{team1, team2, team3, team4, team5};
	}

	public String getName() {
		return name;
	}

	public Team[] getTeams() {
		return teams;
	}

	public int getWorkNr() {
		return work_nr;
	}
	
	public double getPoints() {
		return points;
	}
	
	public void setPoints(double points) {
		this.points = points;
	}
	
	

	public double getWinnings() {
		return winnings;
	}

	public void setWinnings(double winnings) {
		this.winnings = winnings;
	}

	public boolean hasMKIceCream() {
		return hasMKIceCream;
	}

	public void setHasMKIceCream(boolean hasMKIceCream) {
		this.hasMKIceCream = hasMKIceCream;
	}

	@Override
	public int compareTo(Participant participant) {
		// TODO check if direction is correct
		if (compareMode == CompareMode.NAMES) {
			return getName().compareTo(participant.getName());
		} else {
			if (Math.abs(participant.getPoints() - getPoints()) < 0.1) {
				// Points are equal, order by name
				return getName().compareTo(participant.getName());
			} else {
				// Order by points
				return (int)((participant.getPoints() - getPoints())*10);
			}
		}
	}

	public static void setCompareMode(CompareMode compareMode) {
		Participant.compareMode = compareMode;
	}
	
	
}
