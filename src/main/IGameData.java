package main;

import java.util.List;

public interface IGameData {

	
	public List<Participant> getParticipants();
	
	public boolean isIceCreamIncluded();
	
	public String getFilePath();
	
	public String getRemoteFilePath();
	
}
