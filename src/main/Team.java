package main;


public class Team implements Comparable<Team>{


	public static final Team UNKNOWN               = new Team("Unknown");
	
	public static final Team ALGERIA               = new Team("Algeria");
	public static final Team ARGENTINA             = new Team("Argentina");
	public static final Team AUSTRALIA             = new Team("Australia");
	public static final Team BELGIUM               = new Team("Belgium");
	public static final Team BOSNIA_HERZEGOVINA    = new Team("Bosnia and Herzegovina");
	public static final Team BRAZIL                = new Team("Brazil");
	public static final Team CAMEROON              = new Team("Cameroon");
	public static final Team CHILE                 = new Team("Chile");
	public static final Team COLOMBIA              = new Team("Colombia");
	public static final Team COSTA_RICA            = new Team("Costa Rica");
	public static final Team CROATIA               = new Team("Croatia");
	public static final Team COTE_DIVOIRE          = new Team("Côte d'Ivoire");
	public static final Team ECUADOR               = new Team("Ecuador");
	public static final Team ENGLAND               = new Team("England");
	public static final Team FRANCE                = new Team("France");
	public static final Team GERMANY               = new Team("Germany");
	public static final Team GHANA                 = new Team("Ghana");
	public static final Team GREECE                = new Team("Greece");
	public static final Team HONDURAS              = new Team("Honduras");
	public static final Team IRAN                  = new Team("Iran");
	public static final Team ITALY                 = new Team("Italy");
	public static final Team JAPAN                 = new Team("Japan");
	public static final Team KOREA_REPUBLIC        = new Team("Korea Republic");
	public static final Team MEXICO                = new Team("Mexico");
	public static final Team NETHERLANDS           = new Team("Netherlands");
	public static final Team NIGERIA               = new Team("Nigeria");
	public static final Team PORTUGAL              = new Team("Portugal");
	public static final Team RUSSIA                = new Team("Russia");
	public static final Team SPAIN                 = new Team("Spain");
	public static final Team SWITZERLAND           = new Team("Switzerland");
	public static final Team USA                   = new Team("USA");
	public static final Team URUGUAY               = new Team("Uruguay");
	
	
	
	private String name;
	private int totalWins = 0;
	private int totalDraws = 0;
	private int totalBonus = 0;
	private int totalChosen = 0;
	private int totalWinsVsNL = 0;
	
	public int getTotalWinsVsNL() {
		return totalWinsVsNL;
	}
	
	public void reset() {
		
		totalWins = 0;
		totalDraws = 0;
		totalBonus = 0;
		totalChosen = 0;
		totalWinsVsNL = 0;
		
		
	}

	public void incTotalWinsVsNL() {
		this.totalWinsVsNL++;
	}

	private Team(String name) {
		this.name = name;
		this.totalWins = 0;
		this.totalDraws = 0;
		this.totalBonus = 0;
		this.totalChosen = 0;
		this.totalWinsVsNL = 0;
	}

	public String getName() {
		return name;
	}

	public int getTotalWins() {
		return totalWins;
	}

	public void incTotalWins() {
		this.totalWins++;
	}

	public int getTotalDraws() {
		return totalDraws;
	}

	public void incTotalDraws() {
		this.totalDraws++;
	}

	public int getTotalBonus() {
		return totalBonus;
	}

	public void incTotalBonus() {
		this.totalBonus++;
	}
	
	public int getTotalChosen() {
		return totalChosen;
	}

	public void incTotalChosen(int factor) {
		this.totalChosen += factor;
	}

	@Override
	public int compareTo(Team team) {
		if (this.totalChosen > team.totalChosen) {
			return -1;
		}
		else if (this.totalChosen < team.totalChosen) {
			return 1;
		}
		else {
			return this.name.compareTo(team.getName());
		}
	}
	
	public double getTotalPoints() {
		return totalWins + (0.5 * totalDraws) + totalBonus;
	}
	
	
	
}
