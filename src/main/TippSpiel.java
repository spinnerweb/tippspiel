package main;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import out.HtmlGen;

import main.Game.Type;


public class TippSpiel {

	public enum SpielType {
		AVALOQ, PRIVATE
	};
	
	
	
	private List<Participant> participants;
	private List<Game> games;
	private List<Team> teams = null;
	private List<Bonus> bonus = null;
	private static final int BID_CHF = 10;
	private SpielType spielType;
	
	private IGameData gameData;
	
	public IGameData getGameData() {
		return gameData;
	}
	
	
	public static int getBidChf() {
		return BID_CHF;
	}

	public final List<Team> getTeams() {
		return teams;
		
	}
	
	public void generate() {
		try {
			saveToFile(getGameData().getFilePath(), HtmlGen.genHTMLTextContent(this));
		//	FTP.uploadData(getGameData().getFilePath(), getGameData().getRemoteFilePath());
			
			//FileUpload.fileUpload(getGameData().getFilePath(), getGameData().getRemoteFilePath());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public TippSpiel(SpielType spielType) {
		
		this.spielType = spielType;
		gameData = new GameDataAvaloq();
		
		participants = gameData.getParticipants();
		
		initTeams();
		initGames();
		initBonus();
		
		calcPoints();
	} 
	
	
	public SpielType getSpielType() {
		return spielType;
	}


	private void initTeams() {
		teams = new ArrayList<Team>();
		
		
		teams.add(Team.ALGERIA            );
		teams.add(Team.ARGENTINA          );
		teams.add(Team.AUSTRALIA          );
		teams.add(Team.BELGIUM            );
		teams.add(Team.BOSNIA_HERZEGOVINA );
		teams.add(Team.BRAZIL             );
		teams.add(Team.CAMEROON           );
		teams.add(Team.CHILE              );
		teams.add(Team.COLOMBIA           );
		teams.add(Team.COSTA_RICA         );
		teams.add(Team.CROATIA            );
		teams.add(Team.COTE_DIVOIRE       );
		teams.add(Team.ECUADOR            );
		teams.add(Team.ENGLAND            );
		teams.add(Team.FRANCE             );
		teams.add(Team.GERMANY            );
		teams.add(Team.GHANA              );
		teams.add(Team.GREECE             );
		teams.add(Team.HONDURAS           );
		teams.add(Team.IRAN               );
		teams.add(Team.ITALY              );
		teams.add(Team.JAPAN              );
		teams.add(Team.KOREA_REPUBLIC     );
		teams.add(Team.MEXICO             );
		teams.add(Team.NETHERLANDS        );
		teams.add(Team.NIGERIA            );
		teams.add(Team.PORTUGAL           );
		teams.add(Team.RUSSIA             );
		teams.add(Team.SPAIN              );
		teams.add(Team.SWITZERLAND        );
		teams.add(Team.USA                );
		teams.add(Team.URUGUAY            );
		
		for(int i=0; i<teams.size(); i++)
			teams.get(i).reset();
	}
	
	private void initGames() {
		games = new ArrayList<Game>();
		
		// Group games
		games.add(new Game(Type.GROUP, "12 Jun 22:00", Team.BRAZIL, Team.CROATIA, 3, 1));
		games.add(new Game(Type.GROUP, "13 Jun 18:00", Team.MEXICO, Team.CAMEROON, 1, 0));
		games.add(new Game(Type.GROUP, "13 Jun 21:00", Team.SPAIN, Team.NETHERLANDS, 1, 5));
		games.add(new Game(Type.GROUP, "14 Jun 00:00", Team.CHILE, Team.AUSTRALIA, 3, 1));
		games.add(new Game(Type.GROUP, "14 Jun 18:00", Team.COLOMBIA, Team.GREECE, 3, 0));
		games.add(new Game(Type.GROUP, "15 Jun 03:00", Team.COTE_DIVOIRE, Team.JAPAN, 2, 1));
		games.add(new Game(Type.GROUP, "14 Jun 21:00", Team.URUGUAY, Team.COSTA_RICA, 1, 3));
		games.add(new Game(Type.GROUP, "15 Jun 00:00", Team.ENGLAND, Team.ITALY, 1, 2));
		games.add(new Game(Type.GROUP, "15 Jun 18:00", Team.SWITZERLAND, Team.ECUADOR, 2, 1));
		games.add(new Game(Type.GROUP, "15 Jun 21:00", Team.FRANCE, Team.HONDURAS, 3, 0));
		games.add(new Game(Type.GROUP, "16 Jun 00:00", Team.ARGENTINA, Team.BOSNIA_HERZEGOVINA, 2, 1));
		games.add(new Game(Type.GROUP, "16 Jun 18:00", Team.GERMANY, Team.PORTUGAL, 4, 0));
		games.add(new Game(Type.GROUP, "16 Jun 21:00", Team.IRAN, Team.NIGERIA, 0, 0));
		games.add(new Game(Type.GROUP, "17 Jun 00:00", Team.GHANA, Team.USA, 1, 2));
		games.add(new Game(Type.GROUP, "17 Jun 18:00", Team.BELGIUM, Team.ALGERIA, 2, 1));
		games.add(new Game(Type.GROUP, "18 Jun 00:00", Team.RUSSIA, Team.KOREA_REPUBLIC, 1, 1));
		games.add(new Game(Type.GROUP, "17 Jun 21:00", Team.BRAZIL, Team.MEXICO, 0, 0));
		games.add(new Game(Type.GROUP, "19 Jun 00:00", Team.CAMEROON, Team.CROATIA, 0, 4));
		games.add(new Game(Type.GROUP, "18 Jun 21:00", Team.SPAIN, Team.CHILE, 0, 2));
		games.add(new Game(Type.GROUP, "18 Jun 18:00", Team.AUSTRALIA, Team.NETHERLANDS, 2, 3));
		games.add(new Game(Type.GROUP, "19 Jun 18:00", Team.COLOMBIA, Team.COTE_DIVOIRE, 2, 1));
		games.add(new Game(Type.GROUP, "20 Jun 00:00", Team.JAPAN, Team.GREECE, 0, 0));
		games.add(new Game(Type.GROUP, "19 Jun 21:00", Team.URUGUAY, Team.ENGLAND, 2, 1));
		games.add(new Game(Type.GROUP, "20 Jun 18:00", Team.ITALY, Team.COSTA_RICA, 0, 1));
		games.add(new Game(Type.GROUP, "20 Jun 21:00", Team.SWITZERLAND, Team.FRANCE, 2, 5));
		games.add(new Game(Type.GROUP, "21 Jun 00:00", Team.HONDURAS, Team.ECUADOR, 1, 2));
		games.add(new Game(Type.GROUP, "21 Jun 18:00", Team.ARGENTINA, Team.IRAN, 1, 0));
		games.add(new Game(Type.GROUP, "22 Jun 00:00", Team.NIGERIA, Team.BOSNIA_HERZEGOVINA, 1, 0));
		games.add(new Game(Type.GROUP, "21 Jun 21:00", Team.GERMANY, Team.GHANA, 2, 2));
		games.add(new Game(Type.GROUP, "23 Jun 00:00", Team.USA, Team.PORTUGAL, 2, 2));
		games.add(new Game(Type.GROUP, "22 Jun 18:00", Team.BELGIUM, Team.RUSSIA, 1, 0));
		games.add(new Game(Type.GROUP, "22 Jun 21:00", Team.KOREA_REPUBLIC, Team.ALGERIA,2 ,4));
		games.add(new Game(Type.GROUP, "23 Jun 22:00", Team.CAMEROON, Team.BRAZIL, 1, 4));
		games.add(new Game(Type.GROUP, "23 Jun 22:00", Team.CROATIA, Team.MEXICO, 1, 3));
		games.add(new Game(Type.GROUP, "23 Jun 18:00", Team.AUSTRALIA, Team.SPAIN, 0, 3));
		games.add(new Game(Type.GROUP, "23 Jun 18:00", Team.NETHERLANDS, Team.CHILE, 2, 0));
		games.add(new Game(Type.GROUP, "24 Jun 22:00", Team.JAPAN, Team.COLOMBIA, 1, 4));
		games.add(new Game(Type.GROUP, "24 Jun 22:00", Team.GREECE, Team.COTE_DIVOIRE, 2, 1));
		games.add(new Game(Type.GROUP, "24 Jun 18:00", Team.ITALY, Team.URUGUAY, 0, 1));
		games.add(new Game(Type.GROUP, "24 Jun 18:00", Team.COSTA_RICA, Team.ENGLAND, 0, 0));
		games.add(new Game(Type.GROUP, "25 Jun 18:00", Team.NIGERIA, Team.ARGENTINA, 2, 3));
		games.add(new Game(Type.GROUP, "25 Jun 18:00", Team.BOSNIA_HERZEGOVINA, Team.IRAN, 3, 1));
		games.add(new Game(Type.GROUP, "25 Jun 22:00", Team.HONDURAS, Team.SWITZERLAND, 0, 3));
		games.add(new Game(Type.GROUP, "25 Jun 22:00", Team.ECUADOR, Team.FRANCE, 0, 0));
		games.add(new Game(Type.GROUP, "26 Jun 18:00", Team.USA, Team.GERMANY, 0, 1));
		games.add(new Game(Type.GROUP, "26 Jun 18:00", Team.PORTUGAL, Team.GHANA, 2, 1));
		games.add(new Game(Type.GROUP, "26 Jun 22:00", Team.KOREA_REPUBLIC, Team.BELGIUM, 0, 1));
		games.add(new Game(Type.GROUP, "26 Jun 22:00", Team.ALGERIA, Team.RUSSIA, 1, 1));


		// Round of 16
		games.add(new Game(Type.ROUND_OF_16, "28 Jun 18:00", Team.BRAZIL, Team.CHILE, 3, 2));
		games.add(new Game(Type.ROUND_OF_16, "28 Jun 22:00", Team.COLOMBIA, Team.URUGUAY, 2, 0));
		games.add(new Game(Type.ROUND_OF_16, "29 Jun 18:00", Team.NETHERLANDS, Team.MEXICO, 2, 1));
		games.add(new Game(Type.ROUND_OF_16, "29 Jun 22:00", Team.COSTA_RICA, Team.GREECE, 5, 3));
		games.add(new Game(Type.ROUND_OF_16, "30 Jun 18:00", Team.FRANCE, Team.NIGERIA, 2, 0));
		games.add(new Game(Type.ROUND_OF_16, "30 Jun 22:00", Team.GERMANY, Team.ALGERIA, 2, 1));
		games.add(new Game(Type.ROUND_OF_16, "1 Jul 18:00", Team.ARGENTINA, Team.SWITZERLAND, 1, 0));
		games.add(new Game(Type.ROUND_OF_16, "1 Jul 22:00", Team.BELGIUM, Team.USA, 2, 1));


		// Quater Finals
		games.add(new Game(Type.QUATER_FINALS, "4 Jul 18:00", Team.FRANCE, Team.GERMANY, 0, 1));     
		games.add(new Game(Type.QUATER_FINALS, "4 Jul 22:00", Team.BRAZIL, Team.COLOMBIA, 2, 1));     
		games.add(new Game(Type.QUATER_FINALS, "5 Jul 18:00", Team.ARGENTINA, Team.BELGIUM, 1, 0));     
		games.add(new Game(Type.QUATER_FINALS, "5 Jul 22:00", Team.NETHERLANDS, Team.COSTA_RICA, 4, 3));     
		

		games.add(new Game(Type.SEMI_FINALS, "8 Jul 22:00", Team.BRAZIL, Team.GERMANY, 1, 7));     
		games.add(new Game(Type.SEMI_FINALS, "9 Jul 22:00", Team.NETHERLANDS, Team.ARGENTINA, 2, 4));     

		// Finals
		games.add(new Game(Type.THIRD_PLACE, "12 Jul 22:00", Team.BRAZIL, Team.NETHERLANDS, 0, 3)); 
		games.add(new Game(Type.FINALS,      "13 Jul 21:00", Team.GERMANY, Team.ARGENTINA, 1, 0));     
		
	}
	
	private void initBonus() {
		bonus = new ArrayList<Bonus>();
		
		// Bonus points
		bonus.add(new Bonus(Bonus.Type.POINTS, "First team to get a (any type of) red card"	,Team.URUGUAY, "Pereira, red card against Costa Rica")); //"Papastathopoulos, yellow / red card against Poland in the 44. minute"));
		bonus.add(new Bonus(Bonus.Type.POINTS, "First team to miss a penalty"				,Team.FRANCE, "Karim Benzema missed against Switzerland")); //"Karagounis missed against Poland in the 71. minute"));
	
		// Bonus points
		bonus.add(new Bonus(Bonus.Type.ICE_CREAM_MK,  null, Team.UNKNOWN, null));
		bonus.add(new Bonus(Bonus.Type.ICE_CREAM_SIM, null, Team.UNKNOWN, null));
	}
	
	private void calcPoints() {
		double points;
		
		// Update the winning and drawing points of the teams
		for(int j=0; j<games.size(); j++) {
			games.get(j).updateTeamPoints();
		}
		
		for(int j=0; j<bonus.size(); j++) {
			bonus.get(j).updateTeamPoints();
		}
		
		Team[] playerTeams;
		for(int i=0; i<participants.size(); i++) {
			playerTeams = participants.get(i).getTeams();
			points = 0.0;
			for(int j=0; j<playerTeams.length; j++) {
				points += playerTeams[j].getTotalPoints()*(playerTeams.length - j);
				participants.get(i).setPoints(points);
				playerTeams[j].incTotalChosen(playerTeams.length - j);
			}
		}
		
		
		// Ice Cream
		calcMKIceCream();
		
		
	}
	
	private void calcMKIceCream() {
		
		// find the FINAL (backwards for performance)
		Game game = null;
		for (int i=games.size()-1; i>=0; i--) {
			game = games.get(i);
			if (game.getType() == Game.Type.FINALS) {
				if (game.getTeam1() == Team.NETHERLANDS || game.getTeam2() == Team.NETHERLANDS) {
					// Check which participants have Netherlands selected as team 1 or 2
					Team[] thisTeams;
					for (int j=0; j<participants.size(); j++) {
						thisTeams = participants.get(j).getTeams();
						for(int k=0; k<2; k++) {
							if (thisTeams[k] == Team.NETHERLANDS)
							{
								participants.get(j).setHasMKIceCream(true);
								break;
							}
						}
					}
				}
				break;
			}
		}
		
	}
	
	
	public List<Participant> getParticipants() {
		return participants;
	}


	public List<Bonus> getBonus() {
		return bonus;
	}




	public List<Game> getGames() {
		return games;
	}
	
	
//	games.add(new Game(Type.GROUP, "8 Jun 18:00",  Team.POLAND 				,Team.GREECE             ,3	,2));
//	games.add(new Game(Type.GROUP, "8 Jun 20:45",  Team.RUSSIA              ,Team.CZECH_REPUBLIC     ,3	,2));
//	games.add(new Game(Type.GROUP, "9 Jun 18:00",  Team.NETHERLANDS         ,Team.DENMARK            ,3	,2));
//	games.add(new Game(Type.GROUP, "9 Jun 20:45",  Team.GERMANY             ,Team.PORTUGAL           ,3	,2));
//	games.add(new Game(Type.GROUP, "10 Jun 18:00", Team.SPAIN               ,Team.ITALY              ,3	,5));
//	games.add(new Game(Type.GROUP, "10 Jun 20:45", Team.REPUBLIC_OF_IRELAND ,Team.CROATIA            ,3	,2));
//	games.add(new Game(Type.GROUP, "11 Jun 18:00", Team.FRANCE              ,Team.ENGLAND            ,3	,3));
//	games.add(new Game(Type.GROUP, "11 Jun 20:45", Team.UKRAINE             ,Team.SWEDEN             ,3	,2));
//	games.add(new Game(Type.GROUP, "12 Jun 18:00", Team.GREECE              ,Team.CZECH_REPUBLIC     ,3	,2));
//	games.add(new Game(Type.GROUP, "12 Jun 20:45", Team.POLAND              ,Team.RUSSIA             ,3	,3));
//	games.add(new Game(Type.GROUP, "13 Jun 18:00", Team.DENMARK             ,Team.PORTUGAL           ,3	,2));
//	games.add(new Game(Type.GROUP, "13 Jun 20:45", Team.NETHERLANDS         ,Team.GERMANY            ,3	,2));
//	games.add(new Game(Type.GROUP, "14 Jun 18:00", Team.ITALY               ,Team.CROATIA            ,3	,2));
//	games.add(new Game(Type.GROUP, "14 Jun 20:45", Team.SPAIN               ,Team.REPUBLIC_OF_IRELAND,3	,3));
//	games.add(new Game(Type.GROUP, "15 Jun 18:00", Team.UKRAINE             ,Team.FRANCE             ,3	,2));
//	games.add(new Game(Type.GROUP, "15 Jun 20:45", Team.SWEDEN              ,Team.ENGLAND            ,3	,2));
//	games.add(new Game(Type.GROUP, "16 Jun 20:45", Team.GREECE              ,Team.RUSSIA             ,3	,2));
//	games.add(new Game(Type.GROUP, "16 Jun 20:45", Team.CZECH_REPUBLIC      ,Team.POLAND             ,3	,4));
//	games.add(new Game(Type.GROUP, "17 Jun 20:45", Team.PORTUGAL            ,Team.NETHERLANDS        ,3	,2));
//	games.add(new Game(Type.GROUP, "17 Jun 20:45", Team.DENMARK             ,Team.GERMANY            ,3	,2));
//	games.add(new Game(Type.GROUP, "18 Jun 20:45", Team.CROATIA             ,Team.SPAIN              ,3	,2));
//	games.add(new Game(Type.GROUP, "18 Jun 20:45", Team.ITALY               ,Team.REPUBLIC_OF_IRELAND,3	,2));
//	games.add(new Game(Type.GROUP, "19 Jun 20:45", Team.SWEDEN              ,Team.FRANCE             ,3	,2));
//	games.add(new Game(Type.GROUP, "19 Jun 20:45", Team.ENGLAND             ,Team.UKRAINE            ,3	,2));

	
	public void saveToFile(String filename, String input) throws IOException {
		File file = new File(filename);
		file.createNewFile();
		FileWriter fstream = new FileWriter(filename);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write(input);
		out.close();

	}

}
