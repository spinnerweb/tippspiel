package main;

import java.util.ArrayList;
import java.util.List;

public class GameDataAvaloq implements IGameData {

	private List<Participant> participants;
	
	public GameDataAvaloq() {
		participants = new ArrayList<Participant>();
		

		participants.add(new Participant("SIM",   858, true,  Team.BRAZIL,      Team.SPAIN,       Team.GERMANY,     Team.BELGIUM,        Team.NETHERLANDS));
		participants.add(new Participant("MK" ,     4, false, Team.NETHERLANDS, Team.BELGIUM,     Team.ARGENTINA,   Team.GERMANY,        Team.BRAZIL));
		participants.add(new Participant("HEM",   340, true , Team.BRAZIL,      Team.PORTUGAL,    Team.URUGUAY,     Team.BELGIUM,        Team.CHILE));
		participants.add(new Participant("JMI",   617, true , Team.GERMANY,     Team.BRAZIL,      Team.ARGENTINA,   Team.SPAIN,          Team.SWITZERLAND));
		participants.add(new Participant("SEC",   249, true , Team.BRAZIL,      Team.GERMANY,     Team.SPAIN,       Team.BELGIUM,        Team.ARGENTINA));
		participants.add(new Participant("GUM",  1192, true , Team.BRAZIL,      Team.SPAIN,       Team.ARGENTINA, 	Team.FRANCE,     	 Team.GERMANY));
		participants.add(new Participant("GRD",   550, true, Team.ARGENTINA,    Team.BRAZIL,      Team.SPAIN,       Team.GERMANY,        Team.ITALY));
		participants.add(new Participant("WIL",   375, false, Team.SPAIN,       Team.BRAZIL,      Team.ENGLAND, 	Team.NETHERLANDS,    Team.ARGENTINA));
		
		
//		participants.add(new Participant("BIM",  1228, Team.ENGLAND,     Team.PORTUGAL,    Team.SPAIN,       Team.CZECH_REPUBLIC, Team.GERMANY            ));
//		participants.add(new Participant("BMK",  1159, Team.RUSSIA,      Team.GERMANY,     Team.SPAIN,       Team.ENGLAND,        Team.ITALY              ));
//		participants.add(new Participant("DIC",  1278, Team.SPAIN,       Team.GERMANY,     Team.FRANCE,      Team.PORTUGAL,       Team.ITALY              ));
//		participants.add(new Participant("DIO",  1146, Team.GERMANY,     Team.SPAIN,       Team.NETHERLANDS, Team.RUSSIA,         Team.ENGLAND            ));
//		participants.add(new Participant("GUM",  1192, Team.SPAIN,       Team.NETHERLANDS, Team.GERMANY,     Team.FRANCE,         Team.RUSSIA             ));
//		participants.add(new Participant("GRD",   550, Team.SPAIN,       Team.GERMANY,     Team.POLAND,      Team.NETHERLANDS,    Team.FRANCE             ));
//		participants.add(new Participant("HEM",   340, Team.SPAIN,       Team.ENGLAND,     Team.RUSSIA,      Team.GERMANY,        Team.PORTUGAL           ));
//		participants.add(new Participant("HNS",   854, Team.GERMANY,     Team.SPAIN,       Team.FRANCE,      Team.PORTUGAL,       Team.CZECH_REPUBLIC     ));
//		participants.add(new Participant("JMI",   617, Team.GERMANY,     Team.SPAIN,       Team.ENGLAND,     Team.ITALY,          Team.NETHERLANDS        ));
//		participants.add(new Participant("MK" ,     4, Team.NETHERLANDS, Team.SPAIN,       Team.GERMANY,     Team.FRANCE,         Team.POLAND             ));
//		participants.add(new Participant("PUR", 50017, Team.FRANCE,      Team.SPAIN,       Team.RUSSIA,      Team.NETHERLANDS,    Team.GERMANY            ));
//		participants.add(new Participant("RCO",   871, Team.SPAIN,       Team.GERMANY,     Team.ENGLAND,     Team.RUSSIA,         Team.NETHERLANDS        ));
//		participants.add(new Participant("RIO", 50010, Team.SPAIN,       Team.NETHERLANDS, Team.GERMANY,     Team.ITALY,          Team.RUSSIA             ));
//		participants.add(new Participant("SAA",  1124, Team.SPAIN,       Team.PORTUGAL,    Team.ITALY,       Team.GREECE,         Team.REPUBLIC_OF_IRELAND));
//		participants.add(new Participant("SNJ",  1119, Team.SPAIN,       Team.ENGLAND,     Team.GERMANY,     Team.FRANCE,         Team.RUSSIA             ));
//		participants.add(new Participant("SIM",   858, Team.SPAIN,       Team.GERMANY,     Team.RUSSIA,      Team.NETHERLANDS,    Team.ENGLAND            ));
//		participants.add(new Participant("VIA", 50025, Team.GERMANY,     Team.SPAIN,       Team.ITALY,       Team.SWEDEN,         Team.GREECE             ));
//		participants.add(new Participant("WEU",   834, Team.SPAIN,       Team.GERMANY,     Team.FRANCE,      Team.CZECH_REPUBLIC, Team.NETHERLANDS        ));
				
	}
	
	@Override
	public List<Participant> getParticipants() {
		return participants;
	}

	@Override
	public boolean isIceCreamIncluded() {
		return true;
	}

	@Override
	public String getFilePath() {
		return "X:/Projects/Internal/Colombo/_sec_Permanent_Staff/SIM/Tippspiel_2014/index.html";
	}

	@Override
	public String getRemoteFilePath() {
		return "avaloq/index.html";
	}

}
