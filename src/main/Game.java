package main;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Game {
	
	private static final int UNKNOWN = -1;
	
	public enum Type {
		GROUP, ROUND_OF_16, QUATER_FINALS, SEMI_FINALS, FINALS, THIRD_PLACE
	};
	
	private Date start;
	private Team team1;
	private Team team2;
	private Type type;
	
	private int goalsTeam1;
	private int goalsTeam2;
	
	
	public Game(Type type, String startDate, Team team1, Team team2) {
		this(type, startDate, team1, team2, UNKNOWN, UNKNOWN);
	}

	public Game(Type type, String startDate, Team team1, Team team2, int goalsTeam1, int goalsTeam2) {
		SimpleDateFormat sf = new SimpleDateFormat("dd MMM HH:mm");
		
		try {
			this.start = sf.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.team1 = team1;
		this.team2 = team2;
		this.goalsTeam1 = goalsTeam1;
		this.goalsTeam2 = goalsTeam2;
		this.type = type;
		
		
	}
	
	public void updateTeamPoints() {
		if (goalsTeam1 == UNKNOWN || goalsTeam2 == UNKNOWN) {
			// Result not yet known
			return;
		} else {
			if (goalsTeam1 == goalsTeam2) {
				// Draw
				team1.incTotalDraws();
				team2.incTotalDraws();
			} else {
				if (goalsTeam1 > goalsTeam2) {
					team1.incTotalWins();
					if (team2 == Team.NETHERLANDS) {
						team1.incTotalWinsVsNL();
					}
				} 
				else {
					team2.incTotalWins();
					if (team1 == Team.NETHERLANDS) {
						team2.incTotalWinsVsNL();
					}
				}
			}

		}
	}

	public Date getStart() {
		return start;
	}

	public Team getTeam1() {
		return team1;
	}

	public Team getTeam2() {
		return team2;
	}
	
	public int getIntGoalsTeam1() {
		return goalsTeam1;
	}

	public int getIntGoalsTeam2() {
		return goalsTeam2;
	}

	public String getGoalsTeam1() {
		if (goalsTeam1 == UNKNOWN)
			return "&nbsp;";
		else
		return Integer.toString(goalsTeam1);
	}

	public String getGoalsTeam2() {
		if (goalsTeam2 == UNKNOWN)
			return "&nbsp;";
		else
		return Integer.toString(goalsTeam2);
	}
	
	public String toString() {
		return "";
	}
	
//	public double getPointsForTeam(Team team) {
//		if (goalsTeam1 == UNKNOWN || goalsTeam2 == UNKNOWN) {
//			// Result not yet known
//			return 0;
//		} else if (team != team1 && team != team2) {
//			// Team is not part of the game
//			return 0;
//		} else {
//			// Calculate points
//			if (goalsTeam1 == goalsTeam2) {
//				// Draw
//				return 0.5;
//			} else {
//				if (team == team1) {
//					if (goalsTeam1 > goalsTeam2)
//						return 1;
//					else
//						return 0;
//				} else {
//					if (goalsTeam2 > goalsTeam1)
//						return 1;
//					else
//						return 0;
//				}
//			}
//		}
//	}


	
	public Type getType() {
		return type;
	}
	
	
}
