package main;



public class Bonus {
	
	public enum Type {
		POINTS, ICE_CREAM_MK, ICE_CREAM_SIM
	};
	
	private Type type;
	private Team team;
	private String info;
	private String criteria;
	
	
	public Bonus(Type type, String criteria, Team team, String info) {
		this.team = team;
		this.type = type;
		this.info = info;
		this.criteria = criteria;
		
	}
	


	public Team getTeam() {
		return team;
	}



	public String getInfo() {
		return info;
	}

	public void updateTeamPoints() {
		if (team == Team.UNKNOWN) {
			// Result not yet known
			return;
		} else {
			if (type == Type.POINTS)
				team.incTotalBonus();
		}
	}
	
	
	public Type getType() {
		return type;
	}
	
	public String getCriteria() {
		return criteria;
	}
	
}
