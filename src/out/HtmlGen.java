package out;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import main.Bonus;
import main.Game;
import main.Game.Type;
import main.Participant;
import main.Participant.CompareMode;
import main.Team;
import main.TippSpiel;
import main.TippSpiel.SpielType;

public class HtmlGen {

	public HtmlGen() {
		
		
		
		
	}
	
	public static String genHTMLTextContent(TippSpiel tippSpiel){
		

		List<Participant> participants = tippSpiel.getGameData().getParticipants();
		
		// Sort participants by scores
		Participant.setCompareMode(CompareMode.SCORES);
		Collections.sort(participants);		
		
		StringBuilder html = new StringBuilder();
		
		html.append("<html><head><title>Tippspiel WM 2014</title><link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />" +
				"<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">" +
				"<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">" +
				"</head>");
		html.append("<body>");

		html.append("<h1>Tippspiel WM 2014</h1>");
		html.append("<h2>Pot: CHF " + participants.size()*TippSpiel.getBidChf() + ".00</h2>");

		html.append("<hr />");
		html.append("<h2>Tipping Scores</h2>");
		html.append("<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\">");
		html.append("<tr><th>Rank</th><th>Participant</th><th>Points</th><th>Current Dividends</th></tr>");
		
		Participant participant;
		int rank = 0;
		double points = 0.0;
		double previousPoints = -1.0;
		
		int[] ranks = new int[3];
		double[] winningPercents = new double[]{50.0,30.0,20.0};
		double[] winningsPerRank = new double[ranks.length];
		
		int rankIndex = -1;

		
		for(int i=0; i<participants.size(); i++) {
			participant = participants.get(i);
			points = participant.getPoints();
			
			
			if (Math.abs(points - previousPoints) > 0.1) {
				rankIndex++;
				previousPoints = points;
			}
			
			if (rankIndex < ranks.length)
				ranks[rankIndex]++;
			else
				break;
			
		}
		
		int winningIndex = 0;
		
		
		for (int i=0; i<ranks.length; i++) {
			
			double totalWinnings = 0;
			for (int j=winningIndex; j<winningIndex + ranks[i]; j++) {
				if (j >= winningPercents.length) {
					// split the pot across all winners in this rank
					break;
				}
				
				totalWinnings += winningPercents[j];
			}
			if (ranks[i] > 0)
				winningsPerRank[i] = totalWinnings / ranks[i];
				
			winningIndex += ranks[i];
			
		}
		
		rank = 1;
		rankIndex = -1;
		
		points = 0.0;
		previousPoints = -1.0;
		
		int numberOfSameRanks = 0;
		

		NumberFormat formatter = new DecimalFormat("0.00");
		
		
		for(int i=0; i<participants.size(); i++) {
			participant = participants.get(i);
			points = participant.getPoints();
			if (Math.abs(points - previousPoints) > 0.1) {
				rankIndex++;
				rank+= numberOfSameRanks;
				numberOfSameRanks = 0;
			}
			
			double winnings = 0.0;
			
			if (rank <= winningPercents.length) {
				winnings = winningsPerRank[rankIndex] * (double)participants.size() * (double)TippSpiel.getBidChf() / 100.0;
			}
			
			winnings = Math.floor(winnings * 20.0) / 20;

			
			html.append("<tr><td class=\"center\">" + rank + "</td>" +
					    "<td>" + getParticipantNameLink(participant) + "</td>" +
					    "<td class=\"right\" >" + participant.getPoints() + "</td>" +
					    "<td class=\"right\">" + formatter.format(winnings) +  "</td>" +
					    "</tr>");
			
			previousPoints = participant.getPoints();
			numberOfSameRanks++;
		}
		
		SimpleDateFormat sf = new SimpleDateFormat("dd MMM HH:mm");
		
		html.append("</table>\n");
		html.append("<p class=\"small\">Last updated: "+(sf.format(new Date()))+"</p>");
		
		html.append("<hr />\n");
		
		html.append("<h2>Game Scores<h2>");
		
		html.append(getGameTable(tippSpiel, Type.GROUP, "Group Stages"));
		html.append("<br />");
		html.append(getGameTable(tippSpiel, Type.ROUND_OF_16, "Round of 16"));
		html.append("<br />");
		html.append(getGameTable(tippSpiel, Type.QUATER_FINALS, "Quarter Finals"));
		html.append("<br />");
		html.append(getGameTable(tippSpiel, Type.SEMI_FINALS, "Semi Finals"));
		html.append("<br />");
		html.append(getGameTable(tippSpiel, Type.THIRD_PLACE, "Match for third"));
		html.append("<br />");
		html.append(getGameTable(tippSpiel, Type.FINALS, "Finals"));
		html.append("<br />");

		// Bonus points
		html.append(getBonusTable(tippSpiel));
		html.append("<br />");
		html.append("<a href=\"http://www.fifa.com/worldcup/matches/index.html\" class=\"uefa\">Official FIFA Results</a>");
		if (false && tippSpiel.getSpielType() == SpielType.AVALOQ) {
			html.append("<br /><br />");
			html.append("<a href=\"Tippspiel_Rules.pdf\" class=\"uefa\">Official Tippspiel Rules</a>");
		}
		html.append("<br /><br />");
		
		// Sort participants by names
		Participant.setCompareMode(CompareMode.NAMES);
		Collections.sort(participants);		

		html.append("<hr />\n");
		html.append("<h2>Tipping Scores Breakdown</h2>");
		html.append("<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\">");
		html.append("<tr><th>Participant</th><th>Factor</th><th>Teams</th><th>Winning Points</th><th>Drawing Points</th><th>Bonus Points</th>");
		if (tippSpiel.getSpielType() == SpielType.AVALOQ) {
			html.append("<th>Ice Cream from MK</th><th>Ice Cream from SIM</th></tr>");
		}
		
		Team[] participantTeams;
		
		for(int i=0; i<participants.size(); i++) {
			participant = participants.get(i);
			participantTeams = participant.getTeams();
			
			for(int j=0; j<participantTeams.length; j++) {
				double factor = (participantTeams.length - j);
				html.append("<tr>"+(j==0 ? "<td rowspan=" + participantTeams.length + ">" + getParticipantNameLink(participant) + "</td>" : "") +
						        "<td class=\"center\">" + factor + "</td>" + 
			                    "<td>" + participantTeams[j].getName() + "</td>" +
						        "<td class=\"right\">" + factor * participantTeams[j].getTotalWins() + "</td>" +
						        "<td class=\"right\">" + factor/2 * participantTeams[j].getTotalDraws() + "</td>" +
						        "<td class=\"right\">" + factor * participantTeams[j].getTotalBonus() + "</td>");
				if (tippSpiel.getSpielType() == SpielType.AVALOQ) {
					html.append("<td class=\"right\">" + (participantTeams[j] == Team.NETHERLANDS && participant.hasMKIceCream() ? "1.0" : "0.0") + "</td>" +
						        "<td class=\"right\">" + (j == participantTeams.length - 1 ? (double)participantTeams[j].getTotalWinsVsNL() : 0.0) + "</td>");
				}
				html.append("</tr>");
				
			}
			
			previousPoints = participant.getPoints();
		}
		
		html.append("</table>\n");
		
		// Statistics
		html.append("<br />");
		html.append("<hr />\n");
		html.append("<h2>Statistics</h2>");
		//html.append("<div class=\"statistics_div\">");
		if (tippSpiel.getSpielType() == SpielType.AVALOQ) {
			//html.append("<img src=\"dio_thumbs_up.jpg\" class=\"dio\" />");
		}
		html.append("<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\">");
		html.append("<th>Team</th><th>Popularity</th>");
		
		List<Team> teams = tippSpiel.getTeams();
		int chosen;
		int totalChosen = 15 * participants.size();
		
		Collections.sort(teams);
		
		for (int i=0; i < teams.size(); i++) {
			chosen = teams.get(i).getTotalChosen();
			if (chosen != 0) {
				
				int percent = (10000 * teams.get(i).getTotalChosen()) / totalChosen;
				
				html.append("<tr>");
				html.append("<td>" + teams.get(i).getName() + "</td>");
				// Beeeeautiful
				// More performant than string formatter
				html.append("<td align=\"right\">" + percent/100 + "." + (percent % 100 < 10 ? "0" : "") + percent % 100 + "%</td>");
				//html.append("<td align=\"right\">" + teams.get(i).getTotalChosen() + "</td>");
				html.append("</tr>");
			}
		}
	
		html.append("</table>\n");
		
		if (tippSpiel.getSpielType() == SpielType.AVALOQ) {
			//html.append("<img src=\"sim_thumbs_up.jpg\" class=\"sim\" />");
		}
		//html.append("</div>");

		html.append("<br /><p class=\"disclaimer\">Disclaimer: These figures are for information purposes only. They refer to the current status of the games. Final winnings will be informed by SIM at the end of the tournament.");
		html.append("<br /><br />&copy; by DIO &amp; SIM</p>");
		
		html.append("</body>");
		html.append("</html>");
		
		System.out.println(html.toString());
		
		return html.toString();
	}
	
	private static StringBuilder getGameTable(TippSpiel tippSpiel, Type type, String title) {
		StringBuilder html = new StringBuilder();
		
		html.append("<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\" class=\"game_table\">");
		html.append("<tr><th colspan=\"6\">"+title+"</th></tr>");
		
		
		List<Game> games = tippSpiel.getGames();
		Game game;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		
		
		for(int i=0; i<games.size(); i++) {
			
			game = games.get(i);
			
			if (game.getType() == type) {
				html.append("<tr><td class=\"date\">"+dateFormat.format(game.getStart())+"</td><td class=\"time\">"+timeFormat.format(game.getStart())+ "</td><td class=\"team\">"+game.getTeam1().getName()+ "</td><td class=\"goals\">"+game.getGoalsTeam1()+ "</td>" +
					"<td class=\"goals\">"+game.getGoalsTeam2()+ "</td><td class=\"team\">"+game.getTeam2().getName()+ "</td></tr>");
			}
		}
		
		html.append("</table>\n");
		
		
		return html;
	}
	
	private static StringBuilder getParticipantNameLink(Participant participant) {
		StringBuilder html = new StringBuilder();
		
		if (participant.getWorkNr() != 0)
			html.append("<a href=\"http://www.int.avaloq.com/whoiswho/view_user.cfm?pnr=" + participant.getWorkNr() + "\">" + participant.getName() + "</a>");
		else
			html.append(participant.getName());
			
		return html;
	}
	
	private static StringBuilder getBonusTable(TippSpiel tippSpiel) {
		StringBuilder html = new StringBuilder();
		
		html.append("<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\" class=\"game_table\">");
		html.append("<tr><th colspan=\"3\">Bonus Points Results</th></tr>");
		html.append("<tr><th>Bonus Criteria</th><th>Winning Team</th><th>Details</th></tr>");
		
		List<Bonus> bonus= tippSpiel.getBonus();
		
		for (int i=0; i<bonus.size(); i++) {
			if (bonus.get(i).getType() == Bonus.Type.POINTS) {
				String info = bonus.get(i).getInfo();
				if (info == null)
					info = "&nbsp";
				html.append("<tr><td>"+bonus.get(i).getCriteria()+"</td><td>"+bonus.get(i).getTeam().getName()+"</td><td>"+info+"</td></tr>");
			}
			
		}
		
		html.append("</table>\n");
		return html;
	}
}
